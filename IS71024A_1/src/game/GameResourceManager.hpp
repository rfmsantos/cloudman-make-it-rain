#pragma once
#ifndef _GOLD_GAMES_GAME_RESOURCE_MANAGER_H_
#define _GOLD_GAMES_GAME_RESOURCE_MANAGER_H_

#include <string>
#include <SFML\Graphics.hpp>

#ifdef _AUDIO
#include <SFML\Audio.hpp>
#endif

namespace gold {
  namespace games {

    /**
     * A singleton manager class which
     * manages resource provision and lifetime.
     */
    class GameResourceManager
    {
    private:
      static const std::string prefix;

      // Utility class which is used to tag and
      // identify resources for later reuse
      class LoadedResource {
      public:
        // Keep a copy of the provided ID locally
        std::string id;

        // Resource
        enum Type {
          texture,
          font,

#ifdef _AUDIO
          sound
#endif
        } type;

        union {
          sf::Texture* texture;
          sf::Font* font;

#ifdef _AUDIO
          sf::SoundBuffer* sound;
#endif
        } resource;

        LoadedResource(const std::string& id, Type type);
        ~LoadedResource();

        // Distinct destructor
        void destroy();
      };

      mutable std::vector<LoadedResource> loadedResources;

      // Private Constructor.
      // GameResourceManager is only available via the
      // singleton getInstance() interface.
      GameResourceManager();

      /**
       * Finds the requested resource
       * @param resource the resource identifier
       * @return the LoadedResource if found or NULL
       */
      LoadedResource* find(const char* resource) const;

    public:
      ~GameResourceManager();
      
      /**
       * Provides the fully qualified system path for the requested resource.
       * 
       * @param resource the resource identifier.
       * @return a string with the fully qualified system path for the resource.
       *
       * NOTE Although we know that std::string is not efficient
       * due to code pages, it is simpler to use the std::string
       * abstraction for memory lifetime management.
       */
      std::string getResourcePath(const char* resource) const;
      
      /**
       * Provides the requested resource font.
       * 
       * @param resource the resource identifier.
       * @return a Font pointer or NULL if the font resource could not be loaded.
       */
      sf::Font* loadFont(const char* resource) const;

      /**
       * Provides the requested resource texture.
       * 
       * @param resource the resource identifier.
       * @return a Texture pointer or NULL if the texture resource could not be loaded.
       */
      sf::Texture* loadTexture(const char* resource) const;
      
#ifdef _AUDIO
      /**
       * Provides the requested resource sound.
       * 
       * @param resource the resource identifier.
       * @return a SoundBuffer pointer or NULL if the SoundBuffer resource could not be loaded.
       */
      sf::SoundBuffer* loadSoundBuffer(const char* resource) const;
#endif

      // TODO loadAudio => include SFML audio lib and link with them

      // Singleton interface
      static GameResourceManager& getInstance();
    };
  }
}

#endif // _GOLD_GAMES_GAME_RESOURCE_MANAGER_H_