#include "GameResourceManager.hpp"

#include <stdio.h>
#include <string.h>

namespace gold {
  namespace games {

    GameResourceManager::LoadedResource::LoadedResource(const std::string& id, GameResourceManager::LoadedResource::Type type) :
      id(id),
      type(type) {
    }

    GameResourceManager::LoadedResource::~LoadedResource() {
      // Destructor is empty since vector manipulation (e.g
      // vector resize when capacity is full) will call
      // destructor and we do not want to delete the resource.
    }
    
    void GameResourceManager::LoadedResource::destroy() {
      if (type == texture && resource.texture != NULL) {
        delete resource.texture;
      }
#ifdef _AUDIO
      else if (type == sound && resource.sound != NULL) {
        delete resource.sound;
      }
#endif
    }

    // Mapped to a build-stage defined preprocessor macro
    const std::string GameResourceManager::prefix = RESOURCES_DIR;

    GameResourceManager::GameResourceManager() {
      printf("Loading resources from: %s\n", prefix.c_str());

      loadedResources.reserve(10);
    }

    GameResourceManager::~GameResourceManager() {
      for (size_t i = 0; i < loadedResources.size(); ++i) {
        loadedResources[i].destroy();
      }
    }

    std::string GameResourceManager::getResourcePath(const char* resource) const {
      return prefix + resource;
    }

    GameResourceManager::LoadedResource* GameResourceManager::find(const char* resource) const {
      for (size_t i = 0; i < loadedResources.size(); ++i) {
        if (strcmp(loadedResources[i].id.c_str(), resource) == 0) {
          return &loadedResources[i];
        }
      }

      return NULL;
    }

    sf::Texture* GameResourceManager::loadTexture(const char* resource) const {
      LoadedResource* loadedResource = find(resource);
      if (loadedResource != NULL && loadedResource->type == LoadedResource::texture) {
        return loadedResource->resource.texture;
      }

      LoadedResource newlyLoadedResource(resource, LoadedResource::texture);
      newlyLoadedResource.resource.texture = new sf::Texture();

      if (newlyLoadedResource.resource.texture->loadFromFile(getResourcePath(resource))) {
        loadedResources.push_back(newlyLoadedResource);
        return newlyLoadedResource.resource.texture;
      }

      return NULL;
    }
    
    sf::Font* GameResourceManager::loadFont(const char* resource) const {
      LoadedResource* loadedResource = find(resource);
      if (loadedResource != NULL && loadedResource->type == LoadedResource::font) {
        return loadedResource->resource.font;
      }

      LoadedResource newlyLoadedResource(resource, LoadedResource::font);
      newlyLoadedResource.resource.font = new sf::Font();

      if (newlyLoadedResource.resource.font->loadFromFile(getResourcePath(resource))) {
        loadedResources.push_back(newlyLoadedResource);
        return newlyLoadedResource.resource.font;
      }

      return NULL;
    }
    
#ifdef _AUDIO
    sf::SoundBuffer* GameResourceManager::loadSoundBuffer(const char* resource) const {
      LoadedResource* loadedResource = find(resource);
      if (loadedResource != NULL && loadedResource->type == LoadedResource::sound) {
        return loadedResource->resource.sound;
      }

      LoadedResource newlyLoadedResource(resource, LoadedResource::sound);
      newlyLoadedResource.resource.sound = new sf::SoundBuffer();

      if (newlyLoadedResource.resource.sound->loadFromFile(getResourcePath(resource))) {
        loadedResources.push_back(newlyLoadedResource);
        return newlyLoadedResource.resource.sound;
      }

      return NULL;
    }
#endif

    GameResourceManager& GameResourceManager::getInstance() {
      static GameResourceManager _manager;
      return _manager;
    }

  }
}