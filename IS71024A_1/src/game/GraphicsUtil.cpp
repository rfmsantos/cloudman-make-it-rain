#include "GraphicsUtil.hpp"

namespace gold {
  namespace games {

    const float GraphicsUtil::pi = 3.14159265358979f;

    GraphicsUtil::GraphicsUtil(float metersPerPixelRatio) :
      metersPerPixelRatio(metersPerPixelRatio),
      pixelsPerMeterRatio(1.f / metersPerPixelRatio) {
    }

    GraphicsUtil::~GraphicsUtil() {
    }
    
    float GraphicsUtil::getMetersPerPixelRatio() const {
      return metersPerPixelRatio;
    }

    float GraphicsUtil::getPixelsPerMeterRatio() const {
      return pixelsPerMeterRatio;
    }
    
    // NOTE We will be using the (+x, -y) quadrant in Box2D which should naturally translate
    // to SFML by simply inverting the y coordinate since the origin is located at the same
    // point. The following utility functions express this.
    
    sf::Vector2f GraphicsUtil::transformBox2DToScreen(float x, float y) const {
      return sf::Vector2f(x * pixelsPerMeterRatio, -y * pixelsPerMeterRatio);
    }

    sf::Vector2f GraphicsUtil::transformBox2DToScreen(const b2Vec2& vector) const {
      return transformBox2DToScreen(vector.x, vector.y);
    }
      
    b2Vec2 GraphicsUtil::transformScreenToBox2D(float x, float y) const {
      return b2Vec2(x * metersPerPixelRatio, -y * metersPerPixelRatio);
    }

    b2Vec2 GraphicsUtil::transformScreenToBox2D(const sf::Vector2f& vector) const {
      return transformScreenToBox2D(vector.x, vector.y);
    }

    sf::Color GraphicsUtil::getColor(const b2Color& colour, int alpha) {
      return sf::Color((int) (colour.r * 255.f), (int) (colour.g * 255.f), (int) (colour.b * 255.f), alpha);
    }

    float GraphicsUtil::toRadians(float angleInDegrees) {
      static float proportion = (pi / 180.0f);
      return angleInDegrees * proportion;
    }

    float GraphicsUtil::toDegrees(float angleInRadians) {
      static float proportion = (180.0f / pi);
      return angleInRadians * proportion;
    }

    const GraphicsUtil& GraphicsUtil::getDefault() {
      static GraphicsUtil _default(1.0);
      return _default;
    }

  }
}