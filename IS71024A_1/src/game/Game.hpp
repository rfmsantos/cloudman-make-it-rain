#pragma once
#ifndef _GOLD_GAMES_GAME_H_
#define _GOLD_GAMES_GAME_H_

#include <vector>

// -- Forward Declarations

namespace sf {
  class RenderWindow;
  class View;
}

namespace gold {
  namespace games {

    // -- Forward Declarations

    class Game;

    // -- Declarations
    
    /**
     * Abstracts the concept of a state within
     * the game lifetime. Implementation of the
     * State design pattern.
     */
    class GameState {
    private:
      Game* _game;
    
    protected:
      /**
       * @return the parent Game pointer
       */
      Game* getGame();

    public:
      GameState(Game* game);
      virtual ~GameState();

      /**
       * The state process
       *
       * @return the next state
       */
      virtual GameState* operator()() = 0;
    };

    class Game {
    private:
      std::vector<GameState*> _gameStates;
      GameState* _state;

      // Store a reference to the window for later reuse by child states
      sf::RenderWindow& _window;

    public:
      // Enumeration of all possible game states
      enum State {
        start = 0,
        title = 1,
        level1 = 2,
        level2 = 3,
        end = 4
      };

      explicit Game(sf::RenderWindow& window);
      ~Game();

      sf::RenderWindow& getWindow() const;
      GameState* getState(State state) const;

      static const char* getDefaultWindowTitle();

      /**
       * Process the current state in the game loop.
       * Returns upon state execution is complete.
       * Recall to execute next state (if possible).
       */
      void operator()();

      /**
       * Identifies wheter subsequent states need to
       * be evaluated.
       * @return true if states require futher processing, false otherwise
       */
      bool hasNext() const;
    };
    
  }
}

#endif // _GOLD_GAMES_GAME_H_