-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- CloudMan: A Tools and Middleware Project                                  --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- How To Play                                                               --
-------------------------------------------------------------------------------

Use the mouse to move the player representation (CloudMan) within the window.
Click to release the droplet CloudMan is holding.

- Level 1

Release droplets while trying to hit the preset droplets in mid-air.

If the droplet does not hit the ground, it is necessary to force CloudMan to
create a new droplet. Simply click the left or right mouse buttons in order for
CloudMan to generate a droplet at will.

- Level 2

Balance the two clouds.

Similarly to the previous, level, it will be necessary to force CloudMan
to create new droplets upon releasing one.

- General

During gameplay, pressing 'p' will pause the game.
If compiled in debug mode, press '~' to toggle Box2D debug drawing.

Once a level is completed, the title screen will have a trophy at the bottom
showing your achievements.

-------------------------------------------------------------------------------
-- Compilation                                                               --
-------------------------------------------------------------------------------

Use the available Visual Studio solution for compiling on windows. Note that it
may be necessary to setup the pre-processor arguments for resource loading. The
'RESOURCES_DIR' definition should be defined as an absolute path or a relative
path according to the *working directory* from where the game will be launched.

By default, the RESOURCES_DIR points to ./resources/. It may be left as is but
remember to copy the included resources directory and place it next to
generated executable. It is also necessary to launch the executable from the
directory it is placed for relative paths to work accordingly.

In order to play sounds, compile with the _AUDIO macro and ensure that the
following 32-bit version of the library is installed on your system:
http://www.mega-nerd.com/libsndfile/. The necessary DLL file is included in the
lib directory (libsndfile-1.dll) and can be placed next to the executable for
sound playback.

Debug mode compilation will include debugging utilities such as Box2D debug
visualisations.

-------------------------------------------------------------------------------
-- Middleware                                                                --
-------------------------------------------------------------------------------

- SFML (Simple Fast Media Library) - http://www.sfml-dev.org/index.php
-- Used for windows, graphics and audio

- Box2D - http://box2d.org/
-- Used for physics

-------------------------------------------------------------------------------
-- Known Issues                                                              --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Resources                                                                 --
-------------------------------------------------------------------------------

The resources available in 'CloudMan' were freely used from the following
sources:

- http://opengameart.org/
- http://www.webweaver.nu/
- http://www.soundjay.com/

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------